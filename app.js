var net = require('net'),
		util = require('util'),
		events = require('events'),
		assert = require('assert'),
		async = require('async');

function connect(options) {

	// Makes sure the prototypes gets exported too
	if (!(this instanceof connect)) return new connect(options);

	events.EventEmitter.call(this);

	var self = this;

	// Sets the defaults for the self.options if it does not exist
	if('object' === typeof options) {
		self.options = options || {port: 31415, host: 'localhost', 'eol': '\r\n'};
	} else {
		self.options = {port: 31415, host: 'localhost', 'eol': '\r\n'};
	}

	self.options.eol = self.options.eol || '\r\n';

	self.dataChunk = '';

	if(self.options.server) {
		// User passed a server
		self.server = self.options.server;
	} else {
		self.server = net.createConnection(self.options.port || 31415 , self.options.host || 'localhost');
	}

	self.server.on('readable', function() {
		var data = self.dataChunk+self.server.read().toString();

		function checkForEOL(data) {
			var locationOfEOL = data.indexOf(self.options.eol);
			if(locationOfEOL===-1) {
				self.dataChunk = data;
			} else {
				self.emit('command', data.slice(0,locationOfEOL));
				self.dataChunk = data.slice(locationOfEOL+self.options.eol.length);
				checkForEOL(self.dataChunk);
			}
		}

		checkForEOL(data);

	})

	return self;

}

util.inherits(connect, events.EventEmitter);

connect.prototype.send = function(command) {
	this.server.write(command+this.options.eol);
}

connect.prototype.end = function() {
	this.server.end();
}

function check(options, sequence, done) {

	// Connects to the server
	var server = connect(options);

	// Runs the commands
	async.eachSeries(sequence, function(action, cb) {
	
		if(action.response) {
			var numberOfResponses = action.response.join().split(',').length;
		} else {
			var numberOfResponses = 0;
		}

		if(action.command&&!(action.command===null||action.command==='null')) {
			// Sends command if specified
			server.send(action.command);
		}

		if(numberOfResponses===0) {
			cb();
			return;
		}

		var i = 0;
		server.on('command', function listener(res) {
	
			if('function' === typeof action.tester) {
				action.tester(action.response[i], res, i, server);
			} else {
				assert.equal(action.response[i], res);
			}

			i++;
			if(i===numberOfResponses) {
				server.removeListener('command', listener);
				cb();
			}

		})

	}, function() {
		// When finished

		// If the server is specified, leave it open
		if(!options.server) server.end();

		done();
	});

}


// Exporting smtp module
module.exports = parser = {};

parser.check = check;
